namespace Revit_ExportBDS
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TestBDS;Integrated Security=True")
        {
        }

        public virtual DbSet<Elements> Elements { get; set; }
        public virtual DbSet<Parameters> Parameters { get; set; }
        public virtual DbSet<ParamValues> ParamValues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Elements>()
                .HasMany(e => e.ParamValues)
                .WithRequired(e => e.Elements)
                .HasForeignKey(e => e.ElementId);

            modelBuilder.Entity<Parameters>()
                .HasMany(e => e.ParamValues)
                .WithOptional(e => e.Parameters)
                .HasForeignKey(e => e.ParameterId)
                .WillCascadeOnDelete();
        }
    }
}
