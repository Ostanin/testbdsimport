namespace Revit_ExportBDS
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ParamValues
    {
        public long Id { get; set; }

        public long ElementId { get; set; }

        public double? ValueNumeric { get; set; }

        public long? ParameterId { get; set; }

        [StringLength(500)]
        public string ValueStr { get; set; }

        public virtual Elements Elements { get; set; }

        public virtual Parameters Parameters { get; set; }
    }
}
