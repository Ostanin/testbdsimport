﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Revit_ExportBDS.DataSet1TableAdapters;


namespace Revit_ExportBDS
{
    public class BdsExporter
    {
        private Document _doc;

        public BdsExporter(Document doc)
        {
            _doc = doc;
        }

        private string GetDocPath()
        {
            string path = "";
            try
            {
                path = ModelPathUtils.ConvertModelPathToUserVisiblePath(_doc.GetWorksharingCentralModelPath());
                if (string.IsNullOrEmpty(path)) //Добавил
                    path = _doc.PathName;
            }
            catch
            {
                path = _doc.PathName;
            }
            return path;
        }

        public async Task<List<ElementDto>> GetRevitElements()
        {
            var allModelElements = new FilteredElementCollector(_doc).WhereElementIsNotElementType()
                .Where(x => (x.Category != null)).ToList();
            ParametersTableAdapter parameters = new ParametersTableAdapter();
            var modelCategories = allModelElements.GroupBy(x => x.Category.Name).OrderBy(x => x.Key).ToList();
            var elelmentsDto = new List<ElementDto>();
            var parametersDto = new Dictionary<string, ParameterDto>();
            foreach (var cat in modelCategories)
            {
                var category = cat.Key;
                var distByCat = cat
                    .Select(x => new { ((ElementType)_doc.GetElement(x.GetTypeId()))?.FamilyName, x.Name, x }).ToList();
                var categoriesByFamily = distByCat.GroupBy(x => new { x.FamilyName, x.Name }).ToList().Select(x => x.ToList())
                    .ToList();

                foreach (var intstances in categoriesByFamily)
                {
                    var symb = intstances[0];

                    var fName = symb.FamilyName ?? "";
                    if (fName == "3D вид")
                        continue;
                    var typeName = symb.Name;
                    if (category == "Помещения" || category == "Пространства" || category == "Зоны")
                    {
                        var totalName = typeName.Split(' ');
                        if (totalName.Length > 1)
                            typeName = typeName.Replace(totalName[totalName.Length - 1], "").Trim();
                    }

                    if (string.IsNullOrEmpty(fName) && string.IsNullOrEmpty(typeName))
                        continue;
                    for (int i = 0; i < intstances.Count; i++)
                    {
                        var ell = new ElementDto(intstances[i].x.Id.IntegerValue);
                        foreach (Parameter param in intstances[i].x.Parameters)
                        {
                            parametersDto.TryGetValue(param.Definition.Name, out var par);
                            if (par == null)
                            {
                                par = new ParameterDto(param.Definition.Name);
                                par.Id = Convert.ToInt64(parameters.InsertParam(par.Name));
                                parametersDto.Add(par.Name, par);
                            }

                            ParamValue parValue = new ParamValue(par);
                            switch (param.StorageType)
                            {
                                case StorageType.Integer:
                                    parValue.ValueNumeric = param.AsInteger();
                                    break;
                                case StorageType.Double:
                                    parValue.ValueNumeric = param.AsDouble();
                                    break;
                                case StorageType.String:
                                    parValue.ValueStr = param.AsString();
                                    break;
                                case StorageType.ElementId:
                                    parValue.ValueStr = _doc.GetElement(param.AsElementId())?.Name;
                                    break;
                                default:
                                    parValue.ValueStr = param.AsValueString();
                                    break;
                            }
                            ell.ParamValues.Add(parValue);
                        }
                        elelmentsDto.Add(ell);
                    }
                }
            }

            return elelmentsDto;
        }

        public async Task WriteRevitTypeStatistic()
        {
            var elements = await GetRevitElements();

            await Task.Run(() =>
              {
                  ExportData(elements);
              });
        }

        private static void ExportData(List<ElementDto> elements)
        {
            ElementsTableAdapter els = new ElementsTableAdapter();
            ParamValuesTableAdapter paramValues = new ParamValuesTableAdapter();
            Model1 db = new Model1();
            var ss = new Stopwatch();
            ss.Start();
            foreach (var el in elements)
            {
                el.Id = Convert.ToInt64(els.InsertEl(el.RevitId));
                foreach (var paramValue in el.ParamValues)
                    paramValues.Insert(el.Id, paramValue.Parameter.Id, paramValue.ValueStr, paramValue.ValueNumeric);
            }

            ss.Stop();
            MessageBox.Show(ss.Elapsed.TotalSeconds.ToString(), "");
        }
    }

}


