﻿using System.Threading.Tasks;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;

namespace Revit_ExportBDS
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Autodesk.Revit.DB;
    using PluginStatistic;
    using Revit_Lib;
    using Revit_Lib.IO;
    using Revit_Lib.Loger;
    using OperationCanceledException = Autodesk.Revit.Exceptions.OperationCanceledException;

    /// <summary>
    ///    Описание команды
    /// </summary>
    [Plugin(CommandId = 0)]
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class Revit_ExportBDSCommand : IExternalCommand
    {
        /// <summary>
        /// Вызов команды
        /// </summary>
        /// <param name="commandData">Данные команды</param>
        /// <param name="message">Сообщение</param>
        /// <param name="elements">Элементы</param>
        /// <returns>Result operations</returns>
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application;
            var bdsExporter = new BdsExporter(app.ActiveUIDocument.Document);
            try
            {
                //Task.Run(()=>
                //{
                    bdsExporter.WriteRevitTypeStatistic();
                //});
               
                return Result.Succeeded;
            }
            catch (OperationCanceledException)
            {
                return Result.Cancelled;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка выполнения команды", ex);
                return Result.Failed;
            }
        }
    }
}