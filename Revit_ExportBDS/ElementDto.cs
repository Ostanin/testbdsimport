﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Revit_ExportBDS
{
    public class ElementDto
    {
        public long Id { get; set; }
        public int RevitId { get; set; }

        public List<ParamValue> ParamValues { get; set; }

        public ElementDto(int revitId)
        {
            RevitId = revitId;
            ParamValues = new List<ParamValue>();
        }
    }

    public class ParamValue
    {
        public long Id { get; set; }
        public double? ValueNumeric { get; set; }

        public ParamValue(ParameterDto parameter)
        {
            Parameter = parameter;
            ValueNumeric = null;
        }

        public string ValueStr { get; set; }

        public ParameterDto Parameter { get; set; }


    }

    public class ParameterDto
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public ParameterDto(string name)
        {
            Name = name;
        }
    }
}
